﻿using OnlineMedicationApp.BusinessObjects.Enums;
using System.Collections.Generic;

namespace OnlineMedicationApp.BusinessObjects.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Role Role { get; set; }
        public string CaregiverPhone { get; set; }
        public string Recommandation { get; set; }
        public Credentials Credentials { get; set; }
        public MedicationPlan MedicationPlan { get; set; }
        public IList<Activities> Activities { get; set; }
        public IList<MedicationActivity> MedicationActivities { get; set; }
    }
}
