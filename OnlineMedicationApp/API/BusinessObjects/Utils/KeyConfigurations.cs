﻿namespace BusinessObjects.Utils
{
    public class KeyConfigurations
    {
        public string TokenKey { get; set; }
        public int ExpirationPeriodInDays { get; set; }
    }
}
