﻿namespace OnlineMedicationApp.BusinessObjects.Enums
{
    public enum Role
    {
        Patient = 0,
        Caregiver = 1,
        Doctor = 2
    }
}
