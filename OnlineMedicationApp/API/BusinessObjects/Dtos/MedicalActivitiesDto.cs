﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class MedicalActivitiesDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Date { get; set; }
        public bool CorrectlyTaken { get; set; }
    }
}
