﻿using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class UserDtoForTable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public bool HasMedicationPlan { get; set; }
        public string CaregiverPhone { get; set; }
    }
}
