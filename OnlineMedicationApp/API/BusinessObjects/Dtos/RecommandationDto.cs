﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class RecommandationDto
    {
        public string Phone { get; set; }
        public string Recommandation { get; set; }
    }
}
