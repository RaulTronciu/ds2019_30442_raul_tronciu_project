﻿namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class CredentialsDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
    }
}
