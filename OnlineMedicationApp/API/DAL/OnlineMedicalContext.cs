﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using OnlineMedicationApp.BusinessObjects.Models;

namespace DAL
{
    public class OnlineMedicationContext : DbContext, IDesignTimeDbContextFactory<OnlineMedicationContext>
    {
        public OnlineMedicationContext(DbContextOptions options) : base(options)
        {
        }

        public OnlineMedicationContext()
        {
        }

        DbSet<User> Users { get; set; }
        DbSet<Credentials> Credentials { get; set; }
        DbSet<MedicationPlan> MedicationPlans { get; set; }
        //DbSet<Prescription> Prescriptions { get; set; }
        DbSet<MedicationActivity> MedicationActivities { get; set; }
        DbSet<Activities> Activities { get; set; }

        public OnlineMedicationContext CreateDbContext(string[] args)
        {
            var options = new DbContextOptionsBuilder<OnlineMedicationContext>();
            options.UseSqlServer("Server=localhost,1433;Initial Catalog=OnlineMedicationDatabase;User ID=sa;Password=Root12345");
                //"Server=.\\SQLEXPRESS;Database=OnlineMedicationDatabaseCore;Trusted_Connection=True;"
            return new OnlineMedicationContext(options.Options);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne(user => user.Credentials)
                .WithOne(credentials => credentials.User)
                .HasForeignKey<Credentials>(credentials => credentials.Id);

            modelBuilder.Entity<Credentials>()
                .HasOne(credentials => credentials.User)
                .WithOne(user => user.Credentials)
                .HasForeignKey<User>(user => user.Id);

            modelBuilder.Entity<User>()
                .HasOne(user => user.MedicationPlan)
                .WithOne(medicationPlan => medicationPlan.User)
                .HasForeignKey<MedicationPlan>(medicationPlan => medicationPlan.Id);

            modelBuilder.Entity<MedicationPlan>()
                .HasOne(medicationPlan => medicationPlan.User)
                .WithOne(user => user.MedicationPlan)
                .HasForeignKey<User>(user => user.Id);

            //modelBuilder.Entity<MedicationPlan>()
            //    .HasMany(medicationPlan => medicationPlan.Prescription)
            //    .WithOne(prescription => prescription.MedicationPlan)
            //    .HasForeignKey(prescription => prescription.Id);

            //modelBuilder.Entity<Prescription>()
            //    .HasOne(prescription => prescription.MedicationPlan)
            //    .WithMany(medicationPlan => medicationPlan.Prescription)
            //    .HasForeignKey(medicationPlan => medicationPlan.Id);

            modelBuilder.Entity<User>()
                .HasMany(user => user.Activities)
                .WithOne(activity => activity.User)
                .HasForeignKey(activity => activity.Id);

            modelBuilder.Entity<Activities>()
                .HasOne(activity => activity.User)
                .WithMany(user => user.Activities)
                .HasForeignKey(user => user.UserId);

            modelBuilder.Entity<User>()
                .HasMany(user => user.MedicationActivities)
                .WithOne(medicationActivity => medicationActivity.User)
                .HasForeignKey(medicationActivity => medicationActivity.Id);

            modelBuilder.Entity<MedicationActivity>()
                .HasOne(medicationActivity => medicationActivity.User)
                .WithMany(user => user.MedicationActivities)
                .HasForeignKey(user => user.UserId);
        }
    }
}
