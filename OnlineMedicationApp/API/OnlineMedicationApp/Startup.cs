﻿using System.Collections.Generic;
using System.Linq;
using BusinessObjects.Utils;
using DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessLogic.Concrete;
using Swashbuckle.AspNetCore.Swagger;

namespace OnlineMedicationApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var keyConfigurations = new KeyConfigurations();

            services.AddCors(options =>
            {
                options.AddPolicy("DefaultPolicy",
                builder =>
                {
                    builder.WithOrigins("http://localhost:4300", "https://localhost:4300");
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });

            services.AddDbContext<OnlineMedicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            Configuration.Bind("Keys", keyConfigurations);

            services.AddSingleton(keyConfigurations);
            services.AddTransient<ITokenGenerator, TokenGenerator>();
            services.AddTransient<IMapper, Mapper>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICaregiverService, CaregiverService>();
            services.AddTransient<IPatientService, PatientService>();
            services.AddTransient<IDoctorService, DoctorService>();

            services.AddBearerAuthentication(keyConfigurations);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(cfg =>
            {
                cfg.SwaggerDoc("v1", new Info { Title = "", Version = "v1" });
                cfg.AddSecurityDefinition("Bearer",
                    new ApiKeyScheme
                    {
                        In = "header",
                        Description = "Please enter JWT with Bearer into field",
                        Name = "Authorization",
                        Type = "apiKey"
                    });
                cfg.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                   {
                       { "Bearer", Enumerable.Empty<string>() }
                   });
                cfg.DescribeAllEnumsAsStrings();
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseCors("DefaultPolicy");
            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Online Medication App");
            });

            app.UseMvc();
        }
    }
}
