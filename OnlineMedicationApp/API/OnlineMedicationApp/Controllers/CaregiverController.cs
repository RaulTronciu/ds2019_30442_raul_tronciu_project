﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessLogic.Abstract;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineMedicationApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CaregiverController : Controller
    {
        private readonly ICaregiverService _caregiverService;

        public CaregiverController(ICaregiverService caregiverService)
        {
            _caregiverService = caregiverService;
        }

        [Authorize(Roles = "Caregiver")]
        [HttpGet("getPatients")]
        public async Task<ActionResult> Get(string id)
        {
            try
            {
                return await _caregiverService.GetPatientsAsync(id);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
