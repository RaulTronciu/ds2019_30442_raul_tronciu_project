﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessLogic.Abstract;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineMedicationApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [Authorize(Roles = "Patient")]
        [HttpGet("getPatientDetails")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                return await _patientService.GetDetailsAsync(id);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Authorize(Roles = "Patient")]
        [HttpPost("postActivity/{id}")]
        public async Task<ActionResult> Post(string id)
        {
            try
            {
                if (Int32.TryParse(id, out int i))
                {
                    var result = await _patientService.Queue(i);
                    return Ok(result);
                }
                else
                    return StatusCode(500);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
