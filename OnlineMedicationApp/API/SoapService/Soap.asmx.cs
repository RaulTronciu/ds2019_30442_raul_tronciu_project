﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace SoapService
{
    /// <summary>
    /// Summary description for Soap
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Soap : System.Web.Services.WebService
    {
        OnlineMedicationContext _dbContext = new OnlineMedicationContext();

        [WebMethod]
        public List<ActivityDto> GetPatientHistory(string phone)
        {
            var list = _dbContext.Activities.Where(x => x.User.Credential.Phone == phone).ToList().Select(x => 
            {
                return new ActivityDto
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    ActivityName = x.ActivityName,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    NormalActivity = x.NormalActivity
                };
            }).ToList();

            List<ActivityDto> result = new List<ActivityDto>();

            if (list.Count == 0)
            {
                result.Add(new ActivityDto
                {
                    Id = -1,
                    UserId = -1,
                    ActivityName = "Empty",
                    StartDate = "Empty",
                    EndDate = "Empty",
                    NormalActivity = false
                });
            }
            else
                result = list;

            return result;
        }

        [WebMethod]
        public List<MedicalActivityDto> GetPatientMedicalHistory(string phone)
        {
            var list = _dbContext.MedicationActivities.Where(x => x.User.Credential.Phone == phone).ToList().Select(x =>
            {
                return new MedicalActivityDto
                {
                    Id = x.Id,
                    UserId = x.UserId,
                    Date = x.Date,
                    CorrectlyTaken = x.CorrectlyTaken
                };
            }).ToList();

            List<MedicalActivityDto> result = new List<MedicalActivityDto>();

            if (list.Count == 0)
            {
                result.Add(new MedicalActivityDto
                {
                    Id = -1,
                    UserId = -1,
                    Date = "Empty",
                    CorrectlyTaken = false
                });
            }
            else
                result = list;

            return result;
        }

        [WebMethod]
        public bool AddRecommandation(string phone, string recommandation)
        {
            var user = _dbContext.Users.Where(x => x.Credential.Phone == phone).FirstOrDefault();

            if (user == null)
                return false;

            user.Recommandation = recommandation;

            _dbContext.SaveChanges();

            return true;
        }

        [WebMethod]
        public bool AddCondition(bool condition, int id)
        {
            var activity = _dbContext.Activities.Where(x => x.Id == id).FirstOrDefault();

            if (activity == null)
                return false;

            activity.NormalActivity = condition;

            _dbContext.SaveChanges();

            return true;
        }
    }

    public class ActivityDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ActivityName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool NormalActivity { get; set; }
    }

    public class MedicalActivityDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Date { get; set; }
        public bool CorrectlyTaken { get; set; }
    }
}
