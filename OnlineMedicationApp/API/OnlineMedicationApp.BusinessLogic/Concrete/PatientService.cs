﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class PatientService : IPatientService
    {
        private readonly OnlineMedicationContext _dbContext;

        public PatientService(OnlineMedicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ActionResult> GetDetailsAsync(int id)
        {
            var result = new JsonResult("Hah")
            {
                StatusCode = 404
            };

            var user = await _dbContext.Set<User>()
                .Where(l => l.Id == id)
                .FirstOrDefaultAsync();

            if (user != null)
            {
                result = new JsonResult(user)
                {
                    StatusCode = 200
                };
            }

            return result;
        }

        public async Task<ActionResult> Queue(int id)
        {
            ActionResult result = new StatusCodeResult(500);
            result = await SendActivityAsync(id);
            result = await ReceiveActivityAsync(id);
            return result;
        }

        private async Task<ActionResult> SendActivityAsync(int id)
        {
            List<string> activities = new List<string>(new string[] { "Sleeping", "Leaving", "Toileting", "Showering", "Grooming", "Breakfast", "Spare_Time/TV", "Lunch", "Snack"});
            var offset = new Random().Next(-200, 0);
            var activity = new Random().Next(0, 8);
            var date = DateTime.Now;
            var endDate = date.ToString("yyyy-MM-dd HH:mm:ss");
            var startDate = date.AddMinutes(offset).ToString("yyyy-MM-dd HH:mm:ss");
            var message = String.Join("     ", startDate, endDate, activities[activity]);

            var factory = new ConnectionFactory() { HostName = "localhost" };

            using (var connection = factory.CreateConnection())

            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "activity",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: "activity",
                                     basicProperties: null,
                                     body: body);
            }

            return new StatusCodeResult(200);
        }

        private async Task<ActionResult> ReceiveActivityAsync(int id)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            var sleepRule = "Sleeping";
            var leaveRule = "Leaving";
            var oneHourRule = new List<string>() { "Toileting", "Showering", "Grooming" };
            ActionResult result = new StatusCodeResult(500);
            var message = "";

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "activity",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                BasicGetResult queueResult = channel.BasicGet("activity", true);
                if (queueResult != null)
                {
                    message = Encoding.UTF8.GetString(queueResult.Body);
                    result = await CheckRules(message, sleepRule, leaveRule, oneHourRule, id);
                }
            }

            return result;
        }

        private async Task<ActionResult> CheckRules(string message, string sleepRule, string leaveRule, List<string> oneHourRule, int id)
        {
            var data = message.Split("     ");
            data[2] = data[2].Replace(" ", "");

            var startTime = DateTime.Parse(data[0]);
            var endTime = DateTime.Parse(data[1]);
            var normalActivity = true;

            TimeSpan duration = endTime - startTime;

            if (duration.TotalHours > 12)
            {
                if (sleepRule.Equals(data[2]))
                {
                    normalActivity = false;
                }
                else if (leaveRule.Equals(data[2]))
                {
                    normalActivity = false;
                }
            }
            else if (duration.TotalHours >= 1)
            {
                if (oneHourRule.Contains(data[2]))
                {
                    normalActivity = false;
                }
            }

            var user = await _dbContext.Set<User>().Where(x => x.Id == id).FirstOrDefaultAsync();

            if (user == null)
                return new StatusCodeResult(500);

            if (user.Activities == null)
                user.Activities = new List<Activities>();

            var activity = new Activities()
            {
                UserId = id,
                ActivityName = data[2],
                StartDate = startTime.ToString("yyyyMMddHHmmss"),
                EndDate = endTime.ToString("yyyyMMddHHmmss"),
                NormalActivity = normalActivity,
                User = user
            };

            user.Activities.Add(activity);

            await _dbContext.SaveChangesAsync();

            return new StatusCodeResult(200);
        }
    }
}
