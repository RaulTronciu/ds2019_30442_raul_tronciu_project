﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Enums;
using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class DoctorService : IDoctorService
    {
        private readonly OnlineMedicationContext _dbContext;

        public DoctorService(OnlineMedicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ActionResult> GetPatientsAsync()
        {
            var users = await _dbContext.Set<User>()
                .Where(l => l.Role == Role.Patient)
                .ToListAsync();

            var ids = users.Select(x => x.Id).ToList();

            var credentials = await _dbContext.Set<Credentials>()
                .Where(c => ids.Contains(c.Id))
                .ToListAsync();

            var medicalPlans = await _dbContext.Set<MedicationPlan>()
                .Where(m => ids.Contains(m.Id) && m.TreatmentPeriod != null)
                .ToListAsync();

            users.ForEach(x =>
            {
                var credential = credentials
                    .Where(c => c.Id == x.Id).FirstOrDefault();

                var medicalPlan = medicalPlans
                    .Where(m => m.Id == x.Id).FirstOrDefault();

                x.Credentials = credential;
                x.MedicationPlan = medicalPlan;
            });

            var userDto = new List<UserDtoForTable>();

            users.ForEach(user =>
                userDto.Add(
                    new UserDtoForTable
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Phone = user.Credentials.Phone,
                        HasMedicationPlan = user.MedicationPlan != null ? true : false,
                        CaregiverPhone = user.CaregiverPhone != null ? user.CaregiverPhone : "None"
                    })
            );

            return new JsonResult(userDto)
            {
                StatusCode = 200
            };
        }

        public async Task<ActionResult> GetCaregiversAsync()
        {
            var users = await _dbContext.Set<User>()
                .Where(l => l.Role == Role.Caregiver)
                .ToListAsync();

            var ids = users.Select(x => x.Id).ToList();

            var credentials = await _dbContext.Set<Credentials>()
                .Where(c => ids.Contains(c.Id))
                .ToListAsync();

            users.ForEach(x =>
            {
                var credential = credentials
                    .Where(c => c.Id == x.Id).FirstOrDefault();

                x.Credentials = credential;
            });

            var userDto = new List<CaregiverForTable>();

            users.ForEach(user =>
                userDto.Add(
                    new CaregiverForTable
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Phone = user.Credentials.Phone
                    })
            );

            return new JsonResult(userDto)
            {
                StatusCode = 200
            };
        }

        public async Task<ActionResult> UpdatePatientAsync(UpdateEntityDto user)
        {
            var credential = await _dbContext.Set<Credentials>()
                .Where(c => c.Phone == user.Phone).FirstOrDefaultAsync();

            var dbUser = await _dbContext.Set<User>()
                .Where(u => u.Id == credential.Id).FirstOrDefaultAsync();

            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;

            if (user.CaregiverPhone != null)
                dbUser.CaregiverPhone = user.CaregiverPhone;

            await _dbContext.SaveChangesAsync();

            return new JsonResult("")
            {
                StatusCode = 200
            };
        }

        public async Task<ActionResult> UpdateUserAsync(CaregiverForTable user)
        {
            var credential = await _dbContext.Set<Credentials>()
                .Where(c => c.Phone == user.Phone).FirstOrDefaultAsync();

            var dbUser = await _dbContext.Set<User>()
                .Where(u => u.Id == credential.Id).FirstOrDefaultAsync();

            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;

            await _dbContext.SaveChangesAsync();

            return new JsonResult("")
            {
                StatusCode = 200
            };
        }

        public async Task<ActionResult> DeleteUserAsync(string phone)
        {
            var credential = await _dbContext.Set<Credentials>()
                .Where(c => c.Phone == phone).FirstOrDefaultAsync();

            var medPlan = await _dbContext.Set<MedicationPlan>()
                .Where(m => m.Id == credential.Id).FirstOrDefaultAsync();

            var user = await _dbContext.Set<User>()
                .Where(m => m.Id == credential.Id).FirstOrDefaultAsync();

            _dbContext.Set<MedicationPlan>().Remove(medPlan);
            _dbContext.Set<Credentials>().Remove(credential);
            _dbContext.Set<User>().Remove(user);

            await _dbContext.SaveChangesAsync();

            return new JsonResult("")
            {
                StatusCode = 200
            };
        }
    }
}
