﻿using BusinessObjects.Utils;
using Microsoft.IdentityModel.Tokens;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly KeyConfigurations _keyConfigurations;

        public TokenGenerator(KeyConfigurations keyConfigurations)
        {
            _keyConfigurations = keyConfigurations;
        }
        public string GenerateToken(Credentials credentials)
        {
            var key = Encoding.ASCII.GetBytes(_keyConfigurations.TokenKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(
                        ClaimTypes.Role, credentials.User.Role.ToString()),
                    new Claim(
                        ClaimTypes.NameIdentifier, credentials.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(_keyConfigurations.ExpirationPeriodInDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var handler = new JwtSecurityTokenHandler();
            var token = handler.CreateToken(tokenDescriptor);

            return handler.WriteToken(token);
        }
    }
}
