﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Enums;
using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class CaregiverService : ICaregiverService
    {
        private readonly OnlineMedicationContext _dbContext;

        public CaregiverService(OnlineMedicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ActionResult> GetPatientsAsync(string id)
        {
            if (!Int32.TryParse(id, out int idInInt))
            {
                return new JsonResult("")
                {
                    StatusCode = 500
                };
            }

            var credential = await _dbContext.Set<Credentials>()
                .Where(c => c.Id == idInInt).FirstOrDefaultAsync();

            var patientsList = await _dbContext.Set<User>()
                .Where(u => u.Role == Role.Patient && u.CaregiverPhone == credential.Phone)
                .ToListAsync();

            var ids = patientsList.Select(x => x.Id).ToList();

            var credentials = await _dbContext.Set<Credentials>()
                .Where(c => ids.Contains(c.Id))
                .ToListAsync();

            patientsList.ForEach(x =>
            {
                var cred = credentials
                    .Where(c => c.Id == x.Id).FirstOrDefault();

                x.Credentials = cred;
            });

            var userDto = new List<CaregiverForTable>();

            patientsList.ForEach(user =>
                userDto.Add(
                    new CaregiverForTable
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Phone = user.Credentials.Phone
                    })
            );

            return new JsonResult(userDto)
            {
                StatusCode = 200
            };
        }
    }
}
