﻿using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessObjects.Dtos;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface IUserService
    {
        Task<ActionResult> LoginAsync(LoginCredentialsDto loginCredentialsDto);
        Task<ActionResult> RegisterAsync(UserDto userDto);
    }
}
