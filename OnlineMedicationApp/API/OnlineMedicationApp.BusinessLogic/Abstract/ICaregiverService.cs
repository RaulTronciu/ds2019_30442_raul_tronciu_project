﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface ICaregiverService
    {
        Task<ActionResult> GetPatientsAsync(string id);
    }
}
