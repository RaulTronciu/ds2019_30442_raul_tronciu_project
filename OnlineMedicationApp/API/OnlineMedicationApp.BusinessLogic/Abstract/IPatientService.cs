﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface IPatientService
    {
        Task<ActionResult> GetDetailsAsync(int id);
        Task<ActionResult> Queue(int id);
    }
}
