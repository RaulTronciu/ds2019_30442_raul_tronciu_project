﻿using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface ITokenGenerator
    {
        string GenerateToken(Credentials credentials);
    }
}
